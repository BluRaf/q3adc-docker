#!/bin/bash

mkdir --p baseq3
pushd ./baseq3

# Download pak0.pk3
wget "https://archive.org/download/Quake_III_Arena_Id_Software_1999/Quake_III_Arena_Id_Software_1999.iso/Quake3%2Fbaseq3%2Fpak0.pk3" -O pak0.pk3

# Download pak{1,2}.pk3
wget https://files.ioquake3.org/quake3-latest-pk3s.zip
unzip quake3-latest-pk3s.zip
rm quake3-latest-pk3s.zip
mv quake3-latest-pk3s/baseq3/pak1.pk3 .
mv quake3-latest-pk3s/baseq3/pak2.pk3 .
rm -r quake3-latest-pk3s

popd
