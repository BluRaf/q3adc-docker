# Lilium Arena Classic in Docker

Lilium Arena Classic dedicated server in Docker - if you need Q3A 1.16n server for Dreamcast lanparty.

> [!CAUTION]
> Server-side portion of LAC is broken, lol. Just skip this repo and run q3ded from official release.

## Usage

```
$ ./get-pk3.sh
$ docker build . -t q3ded-1.16n
$ docker volume create q3a
$ docker run -it --name q3aded --mount source=q3a,target=/home/quake3/baseq3 q3ded-1.16n
```

## TODO
* Can we use gVisor for some basic isolation?
